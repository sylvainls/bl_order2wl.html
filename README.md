# bl_order2wl.html - Introduction

bl_order2wl.html is a standalone HTML/ECMAScript application to convert a
BrickLink™ XML order into a BrickLink™ XML Wanted List.

By “standalone application” we mean that the user just needs to open the file
bl_order2wl.html in their (recent) browser.  No web server needed.  No other
installation than downloading the files.

bl_ord2wl.html is licenced under GPLv3+.


# Requirements

* NONE!


LEGO and BrickLink are trademarks of the LEGO Group, which does not sponsor,
endorse, or authorize this application.
